<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LawyersController extends Controller
{
    public function index()
    {
        $lawyers = \App\Lawyers::leftJoin('specialities', 'lawyers.speciality_id', '=', 'specialities.id')->select('lawyers.*', 'specialities.speciality')->get();
        return view('lawyers.index', compact('lawyers'));
    }
    
    public function create()
    {
        $cities = \App\City::all();
        $areas = \App\Area::all();
        $specialities = \App\Speciality::all();
        $titles = \App\Title::all();
        return view('lawyers.addLawyer', compact('cities', 'areas', 'specialities', 'titles'));
    }
    
    public function store(Request $request)
    {
        $rules = [
            'title_id' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'firstName_ar' => 'required',
            'lastName_ar' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'email' => 'required|e-mail|unique:lawyers',
            'photo' => 'required',
            'address' => 'required',
            'address_ar' => 'required',
            'city_id' => 'required',
            'area_id' => 'required',
            'speciality_id' => 'required',
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        unset($requestedData['_token']);
        
        $cities = \App\Lawyers::create($requestedData);
        if($cities){
            \Session::flash('successMsg','your data inserted successfully ');
            return redirect('/admin/lawyers')->withInput($requestedData);
        }
    }
    
    
    public function update($id, Request $request)
    {
        $rules = [
            'title_id' => 'required',
            'firstName' => 'required',
            'lastName' => 'required',
            'firstName_ar' => 'required',
            'lastName_ar' => 'required',
            'mobile' => 'required',
            'phone' => 'required',
            'email' => 'required|e-mail',
            'photo' => 'required',
            'address' => 'required',
            'address_ar' => 'required',
            'city_id' => 'required',
            'area_id' => 'required',
            'speciality_id' => 'required',
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        unset($requestedData['_token']);
        
        $lawyers = \App\Lawyers::where('id',$id)->update($requestedData);
        if($lawyers){
            \Session::flash('successMsg','your data updated  successfully ');
            return redirect("/admin/lawyers/edit/$id")->withInput($requestedData);
        }
    }
    
    public function destroy($id)
    {
        $lawyer = \App\Lawyers::destroy($id);
        if($lawyer){
            \Session::flash('successMsg','your data deleted successfully ');
            return redirect('/admin/lawyers');
        }
    }
    
    public function show($id)
    {   
        $cities = \App\City::all();
        $areas = \App\Area::all();
        $specialities = \App\Speciality::all();
        $titles = \App\Title::all();
        $lawyerData = \App\Lawyers::find($id);
        return view('lawyers.addLawyer', compact('lawyerData', 'cities', 'areas', 'specialities', 'titles'));
    }
}
