
@extends('adminLayout.master')
@section('content')

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li><p>YOU ARE HERE</p></li>
                <li><a href="" class="active">settings</a></li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3> dashboard  - <span class="semi-bold">Data</span></h3>
   
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="grid simple">
                        <div class="grid-body no-border">
                            <div class="row">
                                <h4 style="margin-bottom: -7px;"> Legal Related Data  <span class="semi-bold"></span></h4>
                                <hr/>
                                <table class="table no-more-tables">
                                    <tbody>
                                        <tr>
                                            <td> <p> <a href="<?php echo url('admin/title/add'); ?>" style=""> Lawyer Title </a></p>   </td>
                                            <td> <span class="label label-primary"> {{ $titlesCount }} Items </span></td>
                                        </tr>
                                        <tr>
                                            <td> <p> <a href="<?php echo url('admin/speciality/add'); ?>" style=""> Lawyer speciality </a></p>   </td>
                                            <td> <span class="label label-primary"> {{ $specialitiesCount }} Items </span></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="grid simple">
                        <div class="grid-body no-border">
                            <div class="row">
                                <h4 style="margin-bottom: -7px;"> Location Data  <span class="semi-bold"></span></h4>
                                <hr/>
                                <table class="table no-more-tables">
                                    <tbody>
                                        <tr>
                                            <td> <p> <a href="<?php echo url('admin/city/add'); ?>" style=""> States/ Cities </a></p>   </td>
                                            <td> <span class="label label-primary"> {{ $citiesCount }} Items </span></td>
                                        </tr>
                                        <tr>
                                            <td> <p> <a href="<?php echo url('admin/area/add'); ?>" style=""> Areas </a></p>   </td>
                                            <td> <span class="label label-primary">{{ $areasCount }} Items </span></td>
                                        </tr>
                                    </tbody>
                                </table>                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection