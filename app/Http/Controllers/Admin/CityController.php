<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = \App\City::distinct()->select('city_ar','city', 'id')->get();
        return view('settings.addState', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'city' => 'required|unique:cities',
            'city_ar' => 'required'
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        
        unset($requestedData['_token']);
        
        $cities = \App\City::create($requestedData);
        if($cities){
            \Session::flash('successMsg','your data inserted successfully ');
            return redirect('/admin/city/add')->withInput($requestedData);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\city  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities = \App\City::destroy($id);
        if($cities){
            $data = ['msg' => 'success', 'status' => true ];
            echo json_encode($data);
        }
    }
    
    public function show($id)
    {
        $currentCity = \App\City::find($id)->with('areas');
        if($currentCity){
            if(Request::ajax()){
                $data = ['data' => $currentCity, 'status' => true ];
                echo json_encode($data);
            }
            return $currentCity;
        }
    }
}
