<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLawyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('title_id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('firstName_ar');
            $table->string('lastName_ar');
            $table->bigInteger('mobile');
            $table->string('phone');
            $table->string('email');
            $table->char('status', 3)->default('yes');
            $table->string('photo');
            $table->text('notes')->nullable();
            $table->text('address');
            $table->text('address_ar');
            $table->integer('city_id');
            $table->integer('area_id');
            $table->integer('speciality_id');
            $table->foreign('title_id')->references('id')->on('titles')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->foreign('speciality_id')->references('id')->on('specialities')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyers');
    }
}
