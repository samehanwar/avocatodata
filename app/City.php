<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    
    protected $tableName = 'cities';
    protected $fillable = ['city', 'city_ar'];
    
    public function areas(){
        return $this->hasMany('App\Area');
    }
    
}
