
@extends('adminLayout.master')
@section('content')

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li><p>YOU ARE HERE</p></li>
                <li><a href="" class="active">dashboard</a></li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3> Lawyers  - <span class="semi-bold">Data</span></h3>
   
            </div>
            @include('dashboard.addNewItemModal')
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="grid-title no-border">
                            <h4>Lawyers  <span class="semi-bold">List</span></h4>
                            <a href="{{ url('/admin/lawyers/add')}}">
                                <button class="btn btn-info pull-right" > <i class="fa fa-plus"></i> Add New Item </button>
                            </a>
                            @include('partials._success')
                            @include('partials._errors')
                        </div>
                        <div class="grid-body no-border">
                            <div class="row">
                            <br>
                            <table class="table table-bordered no-more-tables">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width:40%">Lawyer Name</th>
                                        <th class="text-center" style="width:22%">Speciality</th>
                                        <th class="text-center" style="width:22%"> status</th>
                                        <th class="text-center" style="width:22%">actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        @if(isset($lawyers) && count($lawyers))
                                            @foreach($lawyers as $lawyer)
                                                <td class="text-center" style="vertical-align: middle;">
                                                    <div class="">
                                                        <div class="col-md-4">
                                                            <div class="" style="margin-top: 7px;">
                                                                <img src="{{ $lawyer->photo }}" width="60" height="60" class="img-responsive img-circle" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <h4> {{ $lawyer->firstName }} {{ $lawyer->lastName }}</h4>
                                                            <p> {{ $lawyer->email }} </p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center" style="vertical-align: middle;">
                                                    {{ $lawyer->speciality }}
                                                </td>
                                                <td class="text-center" style="vertical-align: middle;">
                                                    @if($lawyer->status == 'yes')
                                                        <span class="label label-success">active</span>
                                                    @else
                                                        <span class="label label-warning">suspended!</span>
                                                    @endif                                                    
                                                </td>
                                                <td class="text-center" style="vertical-align: middle;">
                                                    <a href="{{ url('/admin/lawyers/delete', $lawyer->id )}}"><button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
                                                    <a href="{{ url('/admin/lawyers/edit', $lawyer->id )}}"><button class="btn btn-primary"><i class="fa fa-edit"></i></button></a>
                                                </td>
                                            @endforeach
                                        @endif
                                        
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection