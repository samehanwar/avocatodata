<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpecialityController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specialities = \App\Speciality::all();
        return view('settings.addSpeciality', compact('specialities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'speciality' => 'required|unique:specialities',
            'speciality_ar' => 'required',
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        
        unset($requestedData['_token']);
        
        $areas = \App\Speciality::create($requestedData);
        if($areas){
            \Session::flash('successMsg','your data inserted successfully ');
            return redirect('/admin/speciality/add')->withInput($requestedData);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities = \App\Speciality::destroy($id);
        if($cities){
            $data = ['msg' => 'success', 'status' => true ];
            echo json_encode($data);
        }
    }
    

}
