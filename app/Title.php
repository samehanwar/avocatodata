<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
    protected $tableName = 'titles';
    protected $fillable = ['title', 'title_ar'];
    
}
