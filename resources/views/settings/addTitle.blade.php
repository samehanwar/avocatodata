
@extends('adminLayout.master')
@section('content')

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li><p>YOU ARE HERE</p></li>
                <li><a href="" class="active">dashboard</a></li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3> dashboard  - <span class="semi-bold">Add Lawyer Title</span></h3>
   
            </div>
            @include('dashboard.addNewItemModal')
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="grid-title no-border" style="padding: 25px;">
                            <h4>Lawyer Titles  <span class="semi-bold">List</span></h4>
                            @include('partials._success')
                            @include('partials._errors')
                        </div>
                        
                        <div class="grid-body no-border">
                            <form action='/admin/title/store' method="post">
                                {{ csrf_field() }}
                            <div class="row">                                    
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="tab-01">
                                      <li class="active"><a href="#en">English</a></li>
                                      <li><a href="#ar">Arabic</a></li>
                                    </ul>
                                    <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                                    <div class="tab-content" style="border: 1px solid #e5e5e5;">
                                      <div class="tab-pane active" id="en">
                                          
                                        <div class="row column-seperation">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label"> Lawyer Title </label>
                                                    <span class="help">e.g. "perofessor"</span>
                                                    <div class="controls">
                                                        <input type="text" name="title" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label"> Titles List </label>
                                                    <div class="controls" id="listEn" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 7px;height: 220px;overflow-y: scroll;">

                                                        @if(isset($titles) && count($titles) > 0)       
                                                            @foreach($titles as $item)
                                                            <div class="cityContainer" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 6px 10px;margin: 5px;"> 
                                                                <span> {{ $item->title }}  </span>
                                                                <span class="pull-right"> <button type="button" class="removeCityBtn" data-id="{{ $item->id }}" style="border-radius:50%;height: 20px;width: 20px;color: #fff;padding:0px 5px;background: #ef7c61;border: none;font-size: 10px;"> <i class="fa fa-remove"></i> </button></span>
                                                            </div>
                                                            @endforeach
                                                        @endif
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                      <div class="tab-pane" id="ar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label"> اللقب   </label>
                                                    <span class="help">e.g. "استاذ"</span>
                                                    <div class="controls">
                                                        <input type="text" name="title_ar" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label"> قائمة الالقاب </label>
                                                    <div class="controls" id="listAr" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 7px;height: 220px;overflow-y: scroll;">
                                                        
                                                        @if(isset($titles) && count($titles) > 0)       
                                                            @foreach($titles as $item)
                                                            <div class="cityContainer" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 6px 10px;margin: 5px;"> 
                                                                <span> {{ $item->title_ar }}  </span>
                                                                <span class="pull-right"> <button type="button" class="removeCityBtn" data-id="{{ $item->id }}" style="border-radius:50%;height: 20px;width: 20px;color: #fff;padding:0px 5px;background: #ef7c61;border: none;font-size: 10px;"> <i class="fa fa-remove"></i> </button></span>
                                                            </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                        
                                      <p class="pull-right" style="margin-right: 15px;">
                                          <button type="reset" class="btn btn-white btn-cons"> Cancel </button>
                                          <button type="submit" class="btn btn-success btn-cons"> Save </button>
                                      </p>  
                                    </div>
                                  </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection