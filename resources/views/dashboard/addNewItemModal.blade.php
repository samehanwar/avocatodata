
<!-- Modal -->
<div class="modal fade" id="addLawyerItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
        <div class="modal-body" style="background: #fff;padding: 40px;">
          <form method="post" action="">  
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="first-name" class="control-label">First Name:</label>
                          <input type="text" class="form-control" id="first-name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label for="last-name" class="control-label">Last Name:</label>
                           <input type="text" class="form-control" id="last-name">
                        </div>
                    </div>
                </div>
              <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="first-name" class="control-label">Mobile Number:</label>
                          <input type="text" class="form-control" id="first-name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label for="last-name" class="control-label">Tel/Fax NO.:</label>
                           <input type="text" class="form-control" id="last-name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="first-name" class="control-label">Email:</label>
                          <input type="text" class="form-control" id="mobile">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label for="last-name" class="control-label">Tel/Fax NO.:</label>
                           <input type="text" class="form-control" id="phone">
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="first-name" class="control-label">Speciality:</label>
                          <textarea class="form-control"></textarea>
                        </div>
                    </div>
                </div>
              
                <div class="" style="padding: 10px;clear: both;margin: 20px auto;text-align: center; ">
                     <span style="display: inline-block;width: 20%;background: #dedede;padding: 0.5px;">  </span>
                     <span style="background: #0aa699;border-radius: 50%; margin: 0px 10px;width: 15px;height: 15px;display: inline-block;"> </span>
                         <span> Location: </span>
                     <span style="background: #0aa699;border-radius: 50%; margin: 0px 10px;width: 15px;height: 15px;display: inline-block;"> </span>
                     <span style="display: inline-block;width: 20%;background: #dedede;padding: 0.5px;">  </span>
                </div>
               
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="first-name" class="control-label">City:</label>
                          <select class="form-control city-select-single" name="city">
                                <option value="AL">Alabama</option>
                                <option value="CA">Cairo</option>
                                <option value="WY">Wyoming</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                           <label for="last-name" class="control-label">Area:</label>
                           <select class="form-control area-select-single" name="city">
                                <option value="AL">Alabama</option>
                                <option value="CA">Cairo</option>
                                <option value="WY">Wyoming</option>
                          </select>
                        </div>
                    </div>
                </div>
              
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                          <label for="Address" class="control-label">Address:</label>
                          <textarea class="form-control"></textarea>
                        </div>
                    </div>
                </div>
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
