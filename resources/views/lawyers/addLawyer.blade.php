
@extends('adminLayout.master')
@section('content')
    <div class="modal fade" id="myModal" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <h4 class="modal-title">image manager</h4>
                </div>
                <div class="modal-body" > </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li><p>YOU ARE HERE</p></li>
                <li><a href="" class="active">dashboard</a></li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3> Lawyers  - <span class="semi-bold">Data</span></h3>
   
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="grid-title no-border" style="padding: 25px;">
                            <h4>Lawyers  <span class="semi-bold">Data</span></h4>
                            @include('partials._success')
                            @include('partials._errors')
                        </div>
                        <div class="grid-body no-border">
                            <div class="row">
                                @if(isset($lawyerData) && count($lawyerData) > 0) 
                                    <form action='/admin/lawyers/update/{{$lawyerData->id}}' method="post">
                                @else
                                    <form action='/admin/lawyers/store' method="post">
                                @endif
                                
                                {{ csrf_field() }}
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="tab-01">
                                      <li class="active"><a href="#en">English</a></li>
                                      <li><a href="#ar">Arabic</a></li>
                                    </ul>
                                    <div class="tools"> <a href="javascript:;" class="collapse"></a> <a href="#grid-config" data-toggle="modal" class="config"></a> <a href="javascript:;" class="reload"></a> <a href="javascript:;" class="remove"></a> </div>
                                    <div class="tab-content" style="border: 1px solid #e5e5e5;">
                                      <div class="tab-pane active" id="en">
                                        <div class="row column-seperation">
                                            <div class="col-md-12">
                                                <ul class="nav nav-tabs" id="tab-01">
                                                    <li class="active"><a href="#general">General</a></li>
                                                    <li><a href="#location">Location</a></li>
                                                    <li><a href="#speciality">Speciality</a></li>
                                                    <li><a href="#reviews">Reviews</a></li>
                                                    <li><a href="#certi">Certificates</a></li>
                                                    <li><a href="#custom_fields">Custom Fields</a></li>
                                                </ul>
                                                
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="general">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">Title</label>
                                                                <span class="help">e.g. "professor"</span>
                                                                <div class="controls">
                                                                    <select class="form-control area-select-single" name="title_id">
                                                                        <option value="">-- choose Area --</option>
                                                                        @if(isset($titles) && count($titles) > 0)
                                                                            @foreach($titles as $item)
                                                                                <option value="{{ $item->id }}" @if(isset($lawyerData->title_id) && $lawyerData->title_id == $item->id ) selected @endif> {{ $item->title }} </option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">First Name</label>
                                                                <span class="help">e.g. "Joan"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="firstName" value="@if(isset($lawyerData->firstName)) {{$lawyerData->firstName}} @endif" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">Last Name</label>
                                                                <span class="help">e.g. "smith"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="lastName" value="@if(isset($lawyerData->lastName)) {{$lawyerData->lastName}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">Mobile No.</label>
                                                                <span class="help">e.g. "012 5555 5555"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="mobile" value="@if(isset($lawyerData->mobile)) {{$lawyerData->mobile}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">Tele / Fax</label>
                                                                <span class="help">e.g. "02 0000000"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="phone" value="@if(isset($lawyerData->phone)) {{$lawyerData->phone}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">Email</label>
                                                                <span class="help">e.g. "name@domain.com"</span>
                                                                <div class="controls">
                                                                    <input type="text" name="email" class="form-control" value="@if(isset($lawyerData->email)) {{$lawyerData->email}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="form-label">Status</label>
                                                                <div class="radio radio-success" style="margin-top: 15px;">
                                                                    <input id="yes" type="radio" name="status" value="yes"  @if(isset($lawyerData->status) && $lawyerData->status == 'yes') checked="checked" @elseif(!isset($lawyerData->status)) checked="checked"  @endif>
                                                                    <label for="yes">Active</label>
                                                                    <input id="no" type="radio" name="status" value="no" @if(isset($lawyerData->status) && $lawyerData->status == 'no') checked="checked" @endif>
                                                                    <label for="no">Not Active</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    
                                                    <div class="row">
                                                        <input type="hidden" id="fieldup" value="" />
                                                        <div class="col-md-12 image-data" style="margin-bottom: 30px;">
                                                            <label class="form-label">Personal photo </label>
                                                            <button type="button"  class="btn btn-primary btn-lg image_btn" data-toggle="modal" data-target="#myModal">
                                                                <img src="@if(isset($lawyerData->photo)) {{url('/source/')}}/{{$lawyerData->photo}} @else {{url('/assets/img/noimage.png')}} @endif"
                                                                         alt="" width="90" height="90" id="featuredimage"/>  
                                                                <input type="hidden" id="hiddenfield" value="@if(isset($lawyerData->photo)) {{$lawyerData->photo}} @endif" name="photo" />
                                                            </button>
                                                        </div>
                                                    </div>
                                                      
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <label class="form-label">Notes</label>
                                                            <textarea id="text-editor" class="form-control" name="notes" rows="10" placeholder="Enter text ...">@if(isset($lawyerData->notes)) {{$lawyerData->notes}} @endif</textarea>
                                                        </div>
                                                    </div>
                                                  </div>
                                                    
                                                  <div class="tab-pane" id="location">
                                                        <div class="row column-seperation">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">State/ City</label>
                                                                    <span class="help">e.g. "cairo"</span>
                                                                    <div class="controls">
                                                                        <select id="selectCity" class="form-control area-select-single" name="city_id">
                                                                            <option value="">-- choose city --</option>
                                                                            @if(isset($cities) && count($cities) > 0)
                                                                                @foreach($cities as $city)
                                                                                    <option value="{{ $city->id }}" @if(isset($lawyerData->city_id) && $lawyerData->city_id == $city->id ) selected @endif> {{ $city->city }} </option>
                                                                                @endforeach
                                                                            @endif
                                                                      </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label">Area</label>
                                                                    <span class="help">e.g. "nasr city"</span>
                                                                    <div class="controls">
                                                                        <select class="form-control area-select-single" id="areaID" name="area_id"  @if(!isset($lawyerData->area_id)) disabled="disabled" @endif>>
                                                                            <option value="">-- choose Area --</option>
                                                                            @if(isset($areas) && count($areas) > 0)
                                                                                @foreach($areas as $area)
                                                                                    <option value="{{ $area->id }}" @if(isset($lawyerData->area_id) && $lawyerData->area_id == $area->id ) selected @endif> {{ $area->area }} </option>
                                                                                @endforeach
                                                                            @endif
                                                                      </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label class="form-label">Address</label>
                                                                <textarea id="text-editor" name="address" class="form-control" rows="6" placeholder="Enter text ...">@if(isset($lawyerData->address)) {{$lawyerData->address}} @endif</textarea>
                                                            </div>
                                                        </div>
                                          
                                                  </div>
                                                    
                                                    <div class="tab-pane" id="speciality">
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                            <select class="form-control area-select-single" name="speciality_id">
                                                                <option value="">-- choose Area --</option>
                                                                @if(isset($specialities) && count($specialities) > 0)
                                                                    @foreach($specialities as $item)
                                                                        <option value="{{ $item->id }}" @if(isset($lawyerData->speciality_id) && $lawyerData->speciality_id == $item->id ) selected @endif> {{ $item->speciality }} </option>
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                          </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="tab-pane" id="reviews">
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <p> No Data </p>
                                                          </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="tab-pane" id="certi">
                                                        <div class="row">
                                                          <div class="col-md-12">
                                                              <p> No Data </p>
                                                          </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="tab-pane" id="certi">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <p> No Data </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
             
                                                </div>
                                              </div>
                                        </div>
                                      </div>
                                      <div class="tab-pane" id="ar">
                                        <div class="row">
                                          <div class="col-md-12">
                                                <ul class="nav nav-tabs" id="tab-01">
                                                    <li class="active"><a href="#generalAr">General</a></li>
                                                    <li><a href="#locationAr">Location</a></li>
                                                </ul>
                                                
                                                <div class="tab-content">
                                                  <div class="tab-pane active" id="generalAr">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">الاسم الأول </label>
                                                                <span class="help">e.g. "أحمد"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="firstName_ar" value="@if(isset($lawyerData->firstName_ar)) {{$lawyerData->firstName_ar}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="form-label">الاسم الاخير</label>
                                                                <span class="help">e.g. "ابراهيم"</span>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" name="lastName_ar" value="@if(isset($lawyerData->lastName_ar)) {{$lawyerData->lastName_ar}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>                                                
                                                      
                                                  </div>
                                                    
                                                  <div class="tab-pane" id="locationAr">
                                                        
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label class="form-label">العنوان</label>
                                                                <textarea id="text-editor" name="address_ar" class="form-control" rows="6" placeholder="Enter text ...">@if(isset($lawyerData->address_ar)) {{$lawyerData->address_ar}} @endif</textarea>
                                                            </div>
                                                        </div>
                                          
                                                  </div>
                    
                                                    
                                                    
                                                </div>
                                  
                                          </div>
                                        </div>
                                          
                                      </div>
                                        <div class="pull-right" style="margin-right: 15px;">
                                                <button type="reset" class="btn btn-white btn-cons">Cancel</button>
                                                <button type="submit" class="btn btn-success btn-cons"> save </button>
                                          </div>
                                    </div>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection