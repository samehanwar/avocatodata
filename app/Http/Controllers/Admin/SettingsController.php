<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    
    public function index()
    {
        $areasCount = \DB::table('areas')->select('*')->get();
        $citiesCount = \DB::table('cities')->select('*')->get();
        $specialitiesCount = \DB::table('specialities')->select('*')->get();
        $titlesCount = \DB::table('titles')->select('*')->get();
        
        $data = [
            'areasCount' => count($areasCount),
            'citiesCount' => count($citiesCount),
            'specialitiesCount' => count($specialitiesCount),
            'titlesCount' => count($titlesCount)
        ];
        
        return view('settings.index', $data);
    }
    
    public function addState()
    {
        return view('settings.addState');
    }
    
}
