<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lawyers extends Model
{
    protected $tableName = 'lawyers';
    protected $fillable = [
        'title_id', 'firstName', 'lastName', 'firstName_ar', 'lastName_ar', 'mobile', 'phone',
        'email', 'photo', 'address', 'address_ar', 'city_id', 'area_id', 'speciality_id'
        ];
    
    public function specialities()
    {
        return $this->hasMany('App\Speciality');
    }
    
}
