<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function(){
    return redirect('/login');
});

Route::get('/admin', 'Admin\DashboardController@index');
Route::get('/admin/setting', 'Admin\SettingsController@index');

Route::post('/admin/city/store', 'Admin\CityController@store');
Route::post('/admin/city/delete/{id}', 'Admin\CityController@destroy');
Route::get('/admin/city/add', 'Admin\CityController@create');
Route::post('/admin/city/show/{id}', 'Admin\CityController@show');

Route::post('/admin/speciality/store', 'Admin\SpecialityController@store');
Route::post('/admin/speciality/delete/{id}', 'Admin\SpecialityController@destroy');
Route::get('/admin/speciality/add', 'Admin\SpecialityController@create');
Route::post('/admin/speciality/show/{id}', 'Admin\SpecialityController@show');

Route::post('/admin/title/store', 'Admin\TitleController@store');
Route::post('/admin/title/delete/{id}', 'Admin\TitleController@destroy');
Route::get('/admin/title/add', 'Admin\TitleController@create');
Route::post('/admin/title/show/{id}', 'Admin\TitleController@show');

Route::post('/admin/area/store', 'Admin\AreaController@store');
Route::post('/admin/area/delete/{id}', 'Admin\AreaController@destroy');
Route::get('/admin/area/add', 'Admin\AreaController@create');
Route::get('/admin/area/show/{id}', 'Admin\AreaController@show')->name('areashow');


Route::get('/admin/lawyers', 'Admin\LawyersController@index');
Route::get('/admin/lawyers/add', 'Admin\LawyersController@create');
Route::post('/admin/lawyers/store', 'Admin\LawyersController@store');
Route::post('/admin/lawyers/delete/{id}', 'Admin\LawyersController@destroy');
Route::get('/admin/lawyers/edit/{id}', 'Admin\LawyersController@show');
Route::post('/admin/lawyers/update/{id}', 'Admin\LawyersController@update');

Auth::routes();

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
