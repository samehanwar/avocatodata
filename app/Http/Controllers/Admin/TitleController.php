<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TitleController extends Controller
{
                /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titles = \App\Title::all();
        return view('settings.addTitle', compact('titles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|unique:titles',
            'title_ar' => 'required',
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        
        unset($requestedData['_token']);
        
        $areas = \App\Title::create($requestedData);
        if($areas){
            \Session::flash('successMsg','your data inserted successfully ');
            return redirect('/admin/title/add')->withInput($requestedData);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities = \App\Title::destroy($id);
        if($cities){
            $data = ['msg' => 'success', 'status' => true ];
            echo json_encode($data);
        }
    }
}
