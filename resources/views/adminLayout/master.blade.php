<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8" />
        <title> Avocato - dashboard </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel="stylesheet"/>
        <link href="<?php echo url('assets/plugins/pace/pace-theme-flash.css') ?>" rel="stylesheet" type="text/css" media="screen"/>
        <link href="<?php echo url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/plugins/boostrapv3/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/plugins/boostrapv3/css/bootstrap-theme.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/plugins/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/css/animate.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/vendor/dist/css/select2.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/css/style.css') ?>"  rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/css/responsive.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo url('assets/css/custom-icon-set.css" rel="stylesheet') ?>" type="text/css"/>
        <link href="<?php echo url('assets/css/custom.css" rel="stylesheet') ?>" type="text/css"/>
    </head>
    <body class="">
	<div class="header navbar navbar-inverse "> 
  <!-- BEGIN TOP NAVIGATION BAR -->
  <div class="navbar-inner">
	<div class="header-seperation"> 
		<ul class="nav pull-left notifcation-center" id="main-menu-toggle-wrapper" style="display:none">	
		 <li class="dropdown"> <a id="main-menu-toggle" href="#main-menu"  class="" > <div class="iconset top-menu-toggle-white"></div> </a> </li>		 
		</ul>
      <!-- BEGIN LOGO -->	
      <a href=""><h3 style="color:#fff;margin-left: 50px;margin-top: 15px;display: inline-block;"> AVOCATO </h3></a>
      <!-- END LOGO --> 
<!--      <ul class="nav pull-right notifcation-center">	
        <li class="dropdown" id="header_task_bar"> <a href="index.html" class="dropdown-toggle active" data-toggle=""> <div class="iconset top-home"></div> </a> </li>
        <li class="dropdown" id="header_inbox_bar" > <a href="email.html" class="dropdown-toggle" > <div class="iconset top-messages"></div>  <span class="badge" id="msgs-badge">2</span> </a></li>
		<li class="dropdown" id="portrait-chat-toggler" style="display:none"> <a href="#sidr" class="chat-menu-toggle"> <div class="iconset top-chat-white "></div> </a> </li>        
      </ul>-->
      </div>
      <!-- END RESPONSIVE MENU TOGGLER --> 
      <div class="header-quick-nav" > 
      <!-- BEGIN TOP NAVIGATION MENU -->
	  <div class="pull-left"> 
        <ul class="nav quick-section">
          <li class="quicklinks"> <a href="#" class="" id="layout-condensed-toggle" >
            <div class="iconset top-menu-toggle-dark"></div>
            </a> </li>
        </ul>
        <ul class="nav quick-section">
          <li class="quicklinks"> <a href="#" class="" >
            <div class="iconset top-reload"></div>
            </a> </li>
          <li class="quicklinks"> <span class="h-seperate"></span></li>
          <li class="quicklinks"> <a href="#" class="" >
            <div class="iconset top-tiles"></div>
            </a> </li>
			<li class="m-r-10 input-prepend inside search-form no-boarder">
				<span class="add-on"> <span class="iconset top-search"></span></span>
				 <input name="" type="text"  class="no-boarder " placeholder="Search Dashboard" style="width:250px;">
			</li>
		  </ul>
	  </div>
	 <!-- END TOP NAVIGATION MENU -->
	 <!-- BEGIN CHAT TOGGLER -->
         
                        
      <div class="pull-right"> 
		<div class="chat-toggler">	
                        @guest
                        
                        @else
				<a href="#" class="dropdown-toggle" id="my-task-list" data-placement="bottom"  data-content='' data-toggle="dropdown" data-original-title="Notifications">
					<div class="user-details"> 
						<div class="username">
							<span class="badge badge-important"></span> 
							<span class="bold">{{ Auth::user()->name }}</span>									
						</div>						
					</div> 
					<div class="iconset top-down-arrow"></div>
				</a>	
			
				<div class="profile-pic"> 
					<img src="assets/img/profiles/avatar_small.jpg"  alt="" data-src="assets/img/profiles/avatar_small.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="35" height="35" /> 
				</div>       			
			</div>
		 <ul class="nav quick-section ">
			<li class="quicklinks"> 
				<a data-toggle="dropdown" class="dropdown-toggle  pull-right " href="#" id="user-options">						
					<div class="iconset top-settings-dark "></div> 	
				</a>
                            <ul class="dropdown-menu  pull-right" role="menu" aria-labelledby="user-options">
                                <li><a href=""> My Account</a></li>            
                                <li class="divider"></li>                
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    <i class="fa fa-power-off"></i></a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </ul>
			</li> 
			<li class="quicklinks"> <span class="h-seperate"></span></li> 
			<li class="quicklinks"> 	
			<a id="chat-menu-toggle" href="#sidr" class="chat-menu-toggle" ><div class="iconset top-chat-dark "><span class="badge badge-important hide" id="chat-message-count"></span></div>
			</a> 
				
			</li> 
		</ul>
      </div>
                        @endguest
	   <!-- END CHAT TOGGLER -->
      </div> 
      <!-- END TOP NAVIGATION MENU --> 
   
  </div>
  <!-- END TOP NAVIGATION BAR --> 
</div>
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">
  <!-- BEGIN SIDEBAR -->
  <div class="page-sidebar" id="main-menu"> 
  <!-- BEGIN MINI-PROFILE -->
   <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper"> 
   <div class="user-info-wrapper">	
	<div class="profile-wrapper">
		<img src="{{url('assets/img/profiles/avatar.jpg')}}"  alt="" data-src="{{url('assets/img/profiles/avatar.jpg')}}" data-src-retina="assets/img/profiles/avatar2x.jpg" width="69" height="69" />
	</div>
    <div class="user-info">
      <div class="greeting">Welcome</div>
      <div class="username"> <span class="semi-bold">{{ Auth::user()->name }}</span></div>
      <div class="status">Status<a href="#"><div class="status-icon green"></div>Online</a></div>
    </div>
   </div>
  <!-- END MINI-PROFILE -->
   
   <!-- BEGIN SIDEBAR MENU -->	
        <p class="menu-title">BROWSE <span class="pull-right"><a href="javascript:;"><i class="fa fa-refresh"></i></a></span></p>
        <ul>	
            <li class="start active "> <a href="{{url('/admin/')}}"> <i class="icon-custom-home"></i> <span class="title">Dashboard</span> <span class="selected"></span> </a> </li>
            <li class=""> <a href="{{url('/admin/lawyers')}}"> <i class="fa fa-adjust"></i> <span class="title">Lawyers</span> <span class="selected"></span> </a> </li>
            <li class=""> <a href="{{url('/admin/setting')}}"> <i class="fa fa fa-adjust"></i> <span class="title">settings</span> </a> </li>    
        </ul>
        
    </div>
    </div>
  <a href="#" class="scrollup">Scroll</a>
   <div class="footer-widget">		
	<div class="progress transparent progress-small no-radius no-margin">
		<div data-percentage="79%" class="progress-bar progress-bar-success animate-progress-bar" ></div>		
	</div>
	<div class="pull-right">
		<div class="details-status">
		<span data-animation-duration="560" data-value="86" class="animate-number"></span>%
	</div>	
	<a href=""><i class="fa fa-power-off"></i></a></div>
  </div>
  <!-- END SIDEBAR --> 
  <!-- BEGIN PAGE CONTAINER-->
  
  
  
  
    @yield('content')
</div>

	<!-- BEGIN CORE JS FRAMEWORK--> 
        <script src="<?php echo url('assets/plugins/jquery-1.8.3.min.js') ?>" type="text/javascript"></script> 
        <script src="<?php echo url('assets/plugins/boostrapv3/js/bootstrap.min.js') ?>" type="text/javascript"></script> 
        <script src="<?php echo url('assets/plugins/breakpoints.js" type="text/javascript') ?>"></script> 
        <script src="<?php echo url('assets/plugins/jquery-unveil/jquery.unveil.min.js') ?>" type="text/javascript"></script> 
        <script src="<?php echo url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo url('assets/plugins/pace/pace.min.js') ?>" type="text/javascript"></script>  
        <script src="<?php echo url('assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') ?>" type="text/javascript"></script>
        <script src="<?php echo url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo url('assets/js/tabs_accordian.js') ?>" type="text/javascript"></script>
        <!--<script src="<?php echo url('assets/js/form_elements.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo url('assets/js/core.js') ?>" type="text/javascript"></script> 
        <script src="<?php echo url('assets/js/chat.js') ?>" type="text/javascript"></script> 
        <script src="<?php echo url('assets/js/demo.js') ?>" type="text/javascript"></script>
        <script src="<?php echo url('assets/vendor/dist/js/select2.full.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo url('assets/plugins/tinymce/tinymce.min.js') ?>" type="text/javascript"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: ".textarea",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker responsivefilemanager textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons | responsivefilemanager",
                image_advtab: true,
                
                filemanager_crossdomain: true,
                external_filemanager_path:"<?php echo url('/'); ?>/filemanager/",
                external_plugins: { "filemanager" : "<?php echo url('/'); ?>/filemanager/plugin.min.js"},
            });

            function responsive_filemanager_callback(field_id){
            var url=jQuery('#'+field_id).val();
            var ref = window.location.hostname , pro = window.location.protocol ;
            var newref = pro+"//"+ref+"/source/" ;
//            var filename = url.replace(newref,'00');
            var filename = url.substring(url.lastIndexOf('/')+1);
                $('#'+field_id).siblings('div.image-data').find('.newpic').attr('src',url);
                $('#'+field_id).siblings('div.image-data').find('#featuredimage').attr('src',url);
                $('#'+field_id).siblings('div.image-data').find('#hiddenfield').attr('value',filename);
                $('#'+field_id).siblings('div.image-data').find('.pic').attr('value',filename);
                $('#'+field_id).siblings('div.image-data').find('.fieldname').attr('value',filename);
            }
            $.fn.hasAttr = function(name) {  
                return this.attr(name) !== undefined;
            };
            
            
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });    
            
        $(document).ready(function(){
            // show modal for image manager 
            $('#myModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) ;
                var ord = button.closest('div.image-data').siblings('input').attr('id');
                var modal = $(this);
                var recipient = '<iframe width="100%" height="700" src="<?php echo url('/'); ?>/filemanager/dialog.php?type=2&amp;field_id='+ord+'&amp;fldr="></iframe>';
                modal.find('.modal-body').html(recipient);
            });
            
            // button for add new image filed 
            $('.add_btn').on('click',function(e){
                e.preventDefault();
                var num = $('.input-append').length + 1;
                console.log(num);
                var render = '';
                    render += '<div class="row imagefiled"><div class="col-md-12 input-append"><table class="table table-bordered"><tbody>';
                    render += '<input id="fieldID0'+num+'" type="hidden" value="" />';
                    render += '<tr class="image-data">';
                    render += '<td width="20%"><a data-toggle="modal" data-target="#myModal"  class="btn launch" type="button"><img src="<?php echo url('/'); ?>/assets/img/noimage.png" class="newpic" alt="" width="100" height="100" border="1"/></a>';
                    render += '<input type="hidden" class="pic" name="uploaded_photo[]" value="" />';
                    render += '<td style="vertical-align:middle"><input class="fieldname form-control" type="text" value="" /></td>';
                    render += '<td style="vertical-align:middle"><button type="button" class="btn btn-danger btn-del noid"><i class="fa fa-trash"></i> </button></td>';
                    render += '</tr> </tbody></table> </div></div>';
                $('#images').append(render);   
            });
            
    

            var token = $('.form_token').attr('value');
            // delete image item from images 
            $('#images').on('click','.btn-del',function(e){
                e.preventDefault();
                var imageId = $(this).data('id');
                console.log(imageId + token);
                var current_url =  window.location.href ;
                $.ajax({
                   url: '<?php echo url('/'); ?>/deleteimage',
                   data: { id : imageId , _token : token},
                    type:'post',
                    success:function(data){
                       console.log(data);
                        window.location = current_url;
                        $('#tabpanel').children('ul').children('li').removeClass('active');
                        $('#tabpanel').children('ul').find('a').attr('href','#images').parent('li').addClass('active');
                    }
                });
                if(imageId == null || imageId == "undefined"){
                    $(this).parents('.imagefiled').remove();
                    $('.tab-content').find('a').attr('href','#images').parent('li').addClass('active');
                    console.log('that is a delete');
                }
            });
            
            $('#AddNewItem').on('click', function(e){
                e.preventDefault();
                $('#addLawyerItem').modal();
            });
            
            $('.city-select-single').select2({
                width: '100%'
            });
            $('.area-select-single').select2({
                width: '100%'
            });
            
            $('.page-content').on('click', '.removeCityBtn', function(e){
                e.preventDefault();
                var $this = $(this);
                var id = $this.data('id');
                var urlParts = window.location.pathname.split('/');
                var domain = urlParts[urlParts.indexOf('admin')+1];
                var dataItem = $('[data-id="'+id+'"]');
                $.ajax({
                    url : "/admin/"+domain+"/delete/" + id,
                    type: "POST",
                    dataType: 'json',
                    success: function(data){
                        if(data.status == true){
                            $this.parents('.tab-content').find(dataItem).parents('.cityContainer').remove();                            
                        }
                    }
                });
            });
            
            
            $('#selectCity').on('change', function(e){
                e.preventDefault();
                var cityId = $(this).val();
                var urlParts = window.location.pathname.split('/');
                var domain = urlParts[urlParts.indexOf('admin')+1];
                $.ajax({
                    url: '/admin/area/show/'+ cityId,
                    type: "get",
                    dataType: 'json',
                    success: function(response){
                        console.log(response);
                        var outputEn = "", outputAr = "", options = "";
                        if(response.status == true){
                            if(domain == 'lawyers'){
                                for(var x=0; x < response.data.length; x++){
                                    options += '<option value="'+ response.data[x].id +'"> '+ response.data[x].area +' </option>';
                                }
                                $('#areaID').attr('disabled', false);
                                $('#areaID').empty().append(options);
                            }else{
                                for(var i=0; i < response.data.length; i++){
                                    outputEn +=  '<div class="cityContainer" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 6px 10px;margin: 5px;">'; 
                                    outputEn +=  '<span>'+ response.data[i].area +'</span>';
                                    outputEn +=  '<span class="pull-right"> <button type="button" class="removeCityBtn" data-id="'+ response.data[i].id +'" style="border-radius:50%;height: 20px;width: 20px;color: #fff;padding:0px 5px;background: #ef7c61;border: none;font-size: 10px;"> <i class="fa fa-remove"></i> </button></span></div>';

                                    outputAr +=  '<div class="cityContainer" style="border: 1px solid #e5e5e5;border-radius: 2px;padding: 6px 10px;margin: 5px;">'; 
                                    outputAr +=  '<span>'+ response.data[i].area_ar +'</span>';
                                    outputAr +=  '<span class="pull-right"> <button type="button" class="removeCityBtn" data-id="'+ response.data[i].id +'" style="border-radius:50%;height: 20px;width: 20px;color: #fff;padding:0px 5px;background: #ef7c61;border: none;font-size: 10px;"> <i class="fa fa-remove"></i> </button></span></div>';
                                }
                            }
                            
                            $('#listEn').empty().append(outputEn);
                            $('#listAr').empty().append(outputAr);
                        }
                    }
                });
            });
            
        });
        </script>
    </body>
</html>

