<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $tableName = 'specialities';
    protected $fillable = ['speciality', 'speciality_ar'];
    
    
    public function lawyers()
    {
        return $this->belongsTo('App\Lawyers');
    }
    
}
