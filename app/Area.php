<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    
    protected $tableName = 'areas';
    protected $fillable = ['area', 'area_ar', 'city_id'];
    
    public function cities(){
        return $this->belongsTo('App\City');
    }
    
}
