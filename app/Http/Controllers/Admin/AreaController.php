<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $areas = \App\Area::all();
        $cities = \App\City::all();
        return view('settings.addArea', compact('areas', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'area' => 'required|unique:areas',
            'area_ar' => 'required',
            'city_id' => 'required'
        ];
        $this->validate($request, $rules);
        $requestedData = $request->all();
        
        unset($requestedData['_token']);
        
        $areas = \App\Area::create($requestedData);
        if($areas){
            \Session::flash('successMsg','your data inserted successfully ');
            return redirect()->route('areashow', ['id' => $requestedData['city_id']]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cities = \App\Area::destroy($id);
        if($cities){
            $data = ['msg' => 'success', 'status' => true ];
            echo json_encode($data);
        }
    }
    
    public function show($id, Request $request)
    {
        $currentCity = \App\Area::where('city_id', $id)->get();
        $cityId = $id;
        $areas = \App\Area::all();
        $cities = \App\City::all();
        if($request->ajax()){
            $data = ['data' => $currentCity, 'cityId' => $cityId, 'status' => true ];
            echo json_encode($data);
            return;
        }
        return view('settings.addArea', compact('currentCity', 'areas', 'cities', 'cityId'));
    }
}
