
@extends('adminLayout.master')
@section('content')

    <div class="page-content"> 
        <div class="clearfix"></div>
        <div class="content">  
            <ul class="breadcrumb">
                <li><p>YOU ARE HERE</p></li>
                <li><a href="" class="active">dashboard</a></li>
            </ul>
            <div class="page-title"> <i class="icon-custom-left"></i>
                <h3> dashboard  - <span class="semi-bold">Data</span></h3>
   
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="grid simple">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            You are logged in!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection